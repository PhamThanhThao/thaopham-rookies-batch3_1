import math


class Shapes(object):
    """
    This class is used as utils to calculate properties of all shapes
    """
    @staticmethod
    def width_of_square(perimeter):
        return perimeter / 4  
    
    @staticmethod
    def calculate_area_of_square(side):
        return side * 4   

    @staticmethod
    def calculate_area_of_rectangle(side):
        //TODO ...
